package com.example.ismai.Kutuphanem.RestApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApiClient {
    private RestApi mRestApi;
    public RestApiClient(String restApiServiceUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(restApiServiceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mRestApi = retrofit.create(RestApi.class);
    }
    public RestApi getRestApi() {
        return mRestApi;
    }
}
