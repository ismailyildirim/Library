package com.example.ismai.Kutuphanem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.ismai.otogaleri.R;

public class AnaEkran extends AppCompatActivity {
    TextView tv;
    Button kutuphane, kitapEkle, cikis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ana_ekran);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Init();
        setClickListener();
    }

    public void Init() {
        tv = findViewById(R.id.txt_hosgeldiniz);
        String isim = getIntent().getStringExtra("isim");
        tv.setText("HOŞGELDİN " + isim.toUpperCase());
        kutuphane = findViewById(R.id.btn_kutuphanem);
        kitapEkle = findViewById(R.id.btn_kitapEkle);
        cikis = findViewById(R.id.btn_cikis);
    }

    public void setClickListener() {
        kitapEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AnaEkran.this, KitapEkle.class);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
                startActivity(i);
            }
        });
        kutuphane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AnaEkran.this, Kutuphane.class);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                String id = getIntent().getStringExtra("uyeId");
                i.putExtra("uyeId", id);
                startActivity(i);
            }
        });
        cikis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AnaEkran.this, Login.class);
                startActivity(i);
            }
        });
    }
}
