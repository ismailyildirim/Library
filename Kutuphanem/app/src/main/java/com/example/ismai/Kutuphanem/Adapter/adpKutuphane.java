package com.example.ismai.Kutuphanem.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ismai.Kutuphanem.Kutuphane;
import com.example.ismai.Kutuphanem.Login;
import com.example.ismai.Kutuphanem.Model.KutuphanePojo;
import com.example.ismai.Kutuphanem.Model.KutuphanemPojo;
import com.example.ismai.otogaleri.R;

import java.util.ArrayList;
import java.util.List;

import static com.example.ismai.otogaleri.R.drawable.rectangle2;

public class adpKutuphane extends RecyclerView.Adapter<adpKutuphane.tanımla> {
    Context context;
    List<KutuphanemPojo> list;
    MyCallBack myCallback;

    public adpKutuphane(Context context, List<KutuphanemPojo> list,MyCallBack myCallback) {
        this.context = context;
        this.list = list;
        this.myCallback = myCallback;
    }
    public interface MyCallBack {
        void listenerGetPosition(int position,String kitapId);
    }

    @NonNull
    @Override
    public tanımla onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        return new tanımla(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final tanımla holder, final int position) {
        holder.txt_Ad.setText("Adı : " + list.get(position).getKitapAdi());
        holder.txt_Yazar.setText("Yazarı : "+ list.get(position).getYazar());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallback.listenerGetPosition(position,list.get(position).getKitapId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class tanımla extends RecyclerView.ViewHolder {
        TextView txt_Ad;
        TextView txt_Yazar;
        View view;

        public tanımla(View itemView) {
            super(itemView);
            txt_Ad = itemView.findViewById(R.id.ad);
            txt_Yazar = itemView.findViewById(R.id.yazar);
            view = itemView.findViewById(R.id.card_view);
        }
    }
    public void filterList(ArrayList<KutuphanemPojo> filterdList){
       list = filterdList;
       notifyDataSetChanged();
    }
}
