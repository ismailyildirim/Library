package com.example.ismai.Kutuphanem.Model;

public class KutuphanemPojo {
    private String yazar;
    private String kitapAdi;
    private String kitapId;

    public String getKitapId() {
        return kitapId;
    }

    public void setKitapId(String kitapId) {
        this.kitapId = kitapId;
    }

    public void setYazar(String yazar) {
        this.yazar = yazar;
    }

    public String getYazar() {
        return yazar;
    }

    public void setKitapAdi(String kitapAdi) {
        this.kitapAdi = kitapAdi;
    }

    public String getKitapAdi() {
        return kitapAdi;
    }

    @Override
    public String toString() {
        return
                "KutuphanemPojo{" +
                        "yazar = '" + yazar + '\'' +
                        ",kitapAdi = '" + kitapAdi + '\'' +
                        "}";
    }
}
