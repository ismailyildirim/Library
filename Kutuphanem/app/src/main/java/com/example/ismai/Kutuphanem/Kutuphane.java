package com.example.ismai.Kutuphanem;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.ismai.Kutuphanem.Adapter.adpKutuphane;
import com.example.ismai.Kutuphanem.Model.KutuphanemPojo;
import com.example.ismai.otogaleri.R;
import com.example.ismai.Kutuphanem.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Kutuphane extends AppCompatActivity implements adpKutuphane.MyCallBack {
    RecyclerView.LayoutManager layoutManager;
    RecyclerView kutuphane;
    adpKutuphane adp;
    List<KutuphanemPojo> list;
    EditText search;
    Button btnSearch,back;
    adpKutuphane.MyCallBack myCallBack = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kutuphane);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Init();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
                if (search.getText().toString().isEmpty()) {
                    closeKeyboard();
                }
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setVisibility(View.VISIBLE);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Kutuphane.this,AnaEkran.class);
                i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                startActivity(i);
            }
        });
    }
    public void Init(){
        kutuphane = findViewById(R.id.rcy_kutuphane);
        layoutManager = new GridLayoutManager(this, 1);
        kutuphane.setLayoutManager(layoutManager);
        String uyeId = getIntent().getStringExtra("uyeId");
        listeDoldur(uyeId);
        search = findViewById(R.id.search);
        btnSearch = findViewById(R.id.btnSearch);
        search.setVisibility(View.INVISIBLE);
        back = findViewById(R.id.back);
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void filter(String text) {
        ArrayList<KutuphanemPojo> filteredlist = new ArrayList<>();
        for (KutuphanemPojo item : list) {
            if (item.getKitapAdi().toLowerCase().contains(text.toLowerCase()) || item.getYazar().toLowerCase().contains(text.toLowerCase())) {
                filteredlist.add(item);
            }
            adp = new adpKutuphane(getApplicationContext(), filteredlist, myCallBack);
            kutuphane.setAdapter(adp);
        }
    }

    public void listeDoldur(String id) {
        list = new ArrayList<>();
        Call<List<KutuphanemPojo>> call = ManagerAll.getOurInstance().kutuphane(id);
        call.enqueue(new Callback<List<KutuphanemPojo>>() {
            @Override
            public void onResponse(Call<List<KutuphanemPojo>> call, Response<List<KutuphanemPojo>> response) {
                if (response.isSuccessful()) {
                    list = response.body();
                    adp = new adpKutuphane(getApplicationContext(), list, myCallBack);
                    kutuphane.setAdapter(adp);
                }
            }

            @Override
            public void onFailure(Call<List<KutuphanemPojo>> call, Throwable t) {
            }
        });
    }

    @Override
    public void listenerGetPosition(int position,String kitapId) {
        Intent i = new Intent(Kutuphane.this, ItemProperty.class);
        i.putExtra("kitapId", kitapId);
        i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
        i.putExtra("isim",getIntent().getStringExtra("isim"));
        startActivity(i);

    }
}


