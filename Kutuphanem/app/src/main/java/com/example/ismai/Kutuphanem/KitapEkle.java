package com.example.ismai.Kutuphanem;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ismai.Kutuphanem.RestApi.ManagerAll;
import com.example.ismai.Kutuphanem.Model.KutuphanePojo;
import com.example.ismai.otogaleri.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KitapEkle extends AppCompatActivity {
    EditText txt_kitapAdi, txt_yazar,txt_yorum;
    RatingBar ratingBar;
    Button btn_ekle,back;
    RelativeLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        define();
        addBook();
    }
    public void define() {
        txt_kitapAdi = findViewById(R.id.txt_kitapAdi);
        txt_yazar = findViewById(R.id.txt_kitapYazari);
        btn_ekle = findViewById(R.id.btn_ekle);
        txt_yorum = findViewById(R.id.txt_kitapYorum);
        ratingBar = findViewById(R.id.ratingBar);
        layout = findViewById(R.id.disLayout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
            }
        });
        back = findViewById(R.id.back3);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KitapEkle.this,AnaEkran.class);
                i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                startActivity(i);
            }
        });

    }


    public void addBook() {
        btn_ekle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt_kitapAdi.getText().toString().isEmpty() || txt_yazar.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"KİTAP ADI VEYA YAZAR BOŞ BIRAKILAMAZ",Toast.LENGTH_LONG).show();
                }else{
                KutuphanePojo.setKitapAdi(txt_kitapAdi.getText().toString());
                KutuphanePojo.setYazar(txt_yazar.getText().toString());
                KutuphanePojo.setYorum(txt_yorum.getText().toString());
                KutuphanePojo.setRating(ratingBar.getRating()+"");
                Intent i = new Intent(KitapEkle.this,AnaEkran.class);
                String uyeId = getIntent().getStringExtra("uyeId");
                i.putExtra("uyeId",uyeId);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                setApi(uyeId,KutuphanePojo.getKitapAdi(),KutuphanePojo.getYazar(),KutuphanePojo.getYorum(),KutuphanePojo.getRating());
                startActivity(i);
                }
            }
        });
    }
    public void setApi(String uyeId,String ad,String yazar,String yorum,String rating) {
        Call<KutuphanePojo> call = ManagerAll.getOurInstance().kayit(uyeId,ad,yazar,yorum,rating);
        call.enqueue(new Callback<KutuphanePojo>() {
            @Override
            public void onResponse(Call<KutuphanePojo> call, Response<KutuphanePojo> response) {
            }

            @Override
            public void onFailure(Call<KutuphanePojo> call, Throwable t) {
            }
        });
    }
    public void closeKeyboard(){
        View view =this.getCurrentFocus();
        if(view!=null){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

}
