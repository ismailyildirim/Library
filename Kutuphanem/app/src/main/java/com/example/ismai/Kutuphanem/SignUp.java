package com.example.ismai.Kutuphanem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ismai.Kutuphanem.RestApi.ManagerAll;
import com.example.ismai.Kutuphanem.RestApi.Result;
import com.example.ismai.otogaleri.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {
    Button signUp;
    EditText userName, password1, password2, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        define();
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void define() {
        signUp = findViewById(R.id.signUp);
        userName = findViewById(R.id.usernamee);
        password1 = findViewById(R.id.password1);
        password2 = findViewById(R.id.password2);
        email = findViewById(R.id.eMail);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().toString().length() < 3) {
                    userName.setError("Kullanıcı adı en az 3 karakter olmalıdır");
                    requestFocus(userName);
                }
                if (!isEmailValid(email.getText().toString())) {
                    email.setError("hatali Mail");
                } else if (password1.getText().toString().isEmpty() && password1.getText().toString().length() != 6) {
                    password1.setError("Sifre 6 karakter olmali");
                } else if (!password1.getText().toString().equals(password2.getText().toString())) {
                    password2.setError("sifreler eslesmiyor");
                } else {
                    userName.setError(null);
                    email.setError(null);
                    password1.setError(null);
                    password2.setError(null);
                    Toast.makeText(SignUp.this, "Kayıt başarılı", Toast.LENGTH_LONG).show();
                    setApi(userName.getText().toString(), password1.getText().toString(), email.getText().toString());
                    Intent i = new Intent(SignUp.this, Login.class);
                    startActivity(i);
                }

            }
        });
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public void setApi(String kullaniciAdi, String sifre, String email) {
        Call<Result> call = ManagerAll.getOurInstance().kaydol(kullaniciAdi, sifre, email);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
}
