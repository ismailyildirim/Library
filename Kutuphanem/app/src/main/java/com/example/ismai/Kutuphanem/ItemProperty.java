package com.example.ismai.Kutuphanem;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ismai.Kutuphanem.Model.Kitap;
import com.example.ismai.Kutuphanem.RestApi.ManagerAll;
import com.example.ismai.otogaleri.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ItemProperty extends AppCompatActivity {
    TextView kitapAdi, yazar;
    EditText yorum;
    RatingBar ratingBar;
    Button update,delete,back;
    RelativeLayout dislayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_property);
        Init();

    }

    public void Init() {
        kitapAdi = findViewById(R.id.kitapAdi);
        yazar = findViewById(R.id.kitapYazar);
        yorum = findViewById(R.id.kitapYorum);
        ratingBar = findViewById(R.id.kitapRating);
        update = findViewById(R.id.update);
        delete = findViewById(R.id.delete);
        back = findViewById(R.id.back2);
        dislayout = findViewById(R.id.disLayout2);
        dislayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
            }
        });
        setApi(getIntent().getStringExtra("kitapId"));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kitapId = getIntent().getStringExtra("kitapId");
                String uyeId = getIntent().getStringExtra("uyeId");
                sil(uyeId,kitapId);
                Intent i = new Intent(getApplicationContext(),AnaEkran.class);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                i.putExtra("uyeId",uyeId);
                startActivity(i);
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kitapId = getIntent().getStringExtra("kitapId");
                guncelle(kitapId,yorum.getText().toString(),ratingBar.getRating()+"");
                Intent i = new Intent(getApplicationContext(),AnaEkran.class);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
                startActivity(i);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),AnaEkran.class);
                i.putExtra("isim",getIntent().getStringExtra("isim"));
                i.putExtra("uyeId",getIntent().getStringExtra("uyeId"));
                startActivity(i);
            }
        });
    }

    public void setApi(final String kitapId) {
        Call<Kitap> call = ManagerAll.getOurInstance().getir(kitapId);
        call.enqueue(new Callback<Kitap>() {
            @Override
            public void onResponse(Call<Kitap> call, Response<Kitap> response) {
                kitapAdi.setText(response.body().getKitapAdi());
                yazar.setText(response.body().getYazar());
                yorum.setText(response.body().getYorum());
                if(response.body().getRating() != null)
                ratingBar.setRating(Float.valueOf(response.body().getRating()));
            }

            @Override
            public void onFailure(Call<Kitap> call, Throwable t) {

            }
        });
    }
    public void guncelle(String kitapId,String yorum,String rating){
        Call<Kitap> call = ManagerAll.getOurInstance().guncelle(kitapId,yorum,rating);
        call.enqueue(new Callback<Kitap>() {
            @Override
            public void onResponse(Call<Kitap> call, Response<Kitap> response) {

            }

            @Override
            public void onFailure(Call<Kitap> call, Throwable t) {

            }
        });
    }

    public void sil(String uyeId, String kitapId){
        Call<Kitap> call = ManagerAll.getOurInstance().sil(uyeId,kitapId);
        call.enqueue(new Callback<Kitap>() {
            @Override
            public void onResponse(Call<Kitap> call, Response<Kitap> response) {

            }

            @Override
            public void onFailure(Call<Kitap> call, Throwable t) {
            }
        });
    }
    public void closeKeyboard(){
        View view =this.getCurrentFocus();
        if(view!=null){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
}
