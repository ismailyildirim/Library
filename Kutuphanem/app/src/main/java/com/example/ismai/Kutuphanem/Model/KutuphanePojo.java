package com.example.ismai.Kutuphanem.Model;

public class KutuphanePojo {
    private static String kitapAdi;
    private static String yazar;
    private static String yorum;
    private static String rating;

    public static String getYorum() {
        return yorum;
    }

    public static void setYorum(String yorum) {
        KutuphanePojo.yorum = yorum;
    }

    public static String getRating() {
        return rating;
    }

    public static void setRating(String rating) {
        KutuphanePojo.rating = rating;
    }

    public static String getKitapAdi() {
        return kitapAdi;
    }

    public static void setKitapAdi(String kitapAdi) {
        KutuphanePojo.kitapAdi = kitapAdi;
    }

    public static String getYazar() {
        return yazar;
    }

    public static void setYazar(String yazar) {
        KutuphanePojo.yazar = yazar;
    }
}
