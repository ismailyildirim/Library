package com.example.ismai.Kutuphanem.RestApi;


import com.example.ismai.Kutuphanem.Model.Kitap;
import com.example.ismai.Kutuphanem.Model.KutuphanePojo;
import com.example.ismai.Kutuphanem.Model.KutuphanemPojo;
import com.example.ismai.Kutuphanem.Model.LoginPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestApi {
    @FormUrlEncoded
    @POST(Url.login)
    Call<LoginPojo> Login(@Field("kad") String ad, @Field("sifre") String sifre);

    @FormUrlEncoded
    @POST(Url.kayit)
    Call<KutuphanePojo> KutuphaneKayit(@Field("uyeId") String uyeId,@Field("kitapAdi") String kitapAdi, @Field("yazar") String yazar,@Field("yorum") String yorum,@Field("rating") String rating);

    @FormUrlEncoded
    @POST(Url.kutuphane)
    Call<List<KutuphanemPojo>> Kutuphanem(@Field("uyeId") String uyeId);

    @FormUrlEncoded
    @POST(Url.kaydol)
    Call<Result> kaydol(@Field("kullaniciAdi") String kullaniciAdi,@Field("sifre") String sifre,@Field("email") String email);

    @FormUrlEncoded
    @POST(Url.duzenle)
    Call<Kitap> getir(@Field("kitap_id") String kitapId);

    @FormUrlEncoded
    @POST(Url.guncelle)
    Call<Kitap> guncelle(@Field("kitap_id") String kitapId,@Field("yorum") String yorum,@Field("rating") String rating);

    @FormUrlEncoded
    @POST(Url.sil)
    Call<Kitap> sil(@Field("uye_id") String uye_id,@Field("kitap_id") String kitap_id);

}
