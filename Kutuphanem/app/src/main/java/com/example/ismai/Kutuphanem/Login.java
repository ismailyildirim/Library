package com.example.ismai.Kutuphanem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.ismai.Kutuphanem.Model.LoginPojo;
import com.example.ismai.otogaleri.R;
import com.example.ismai.Kutuphanem.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    EditText txtUserName, txtPassword;
    Button btnLogin;
    Switch remember;
    String username, password;
    Boolean isChecked;
    Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        define();
        getRemember();
    }

    public void define() {
        txtUserName = findViewById(R.id.userName);
        txtPassword = findViewById(R.id.password);
        remember = findViewById(R.id.remember);
        btnLogin = findViewById(R.id.login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = txtUserName.getText().toString().trim();
                password = txtPassword.getText().toString().trim();
                setRemember();
                setApi(username, password);
            }
        });
        signUp = findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent i = new Intent(Login.this,SignUp.class);
            startActivity(i);
            }
        });
    }

    public void setApi(final String ad, final String sifre) {
        Call<LoginPojo> call = ManagerAll.getOurInstance().giris(ad, sifre);
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.body().getKadi() != null) {
                    Intent i = new Intent(Login.this, AnaEkran.class);
                    i.putExtra("isim", ad);
                    i.putExtra("uyeId", response.body().getId().toString());
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(),"HATALI KULLANICI ADI VEYA SIFRE",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
            }
        });
    }

    public void getRemember() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String savedUserName = sharedPref.getString("username", "");
        String savedPassword = sharedPref.getString("password", "");
        Boolean savedRemember = sharedPref.getBoolean("isChecked", false);
        txtUserName.setText(savedUserName);
        txtPassword.setText(savedPassword);
        remember.setChecked(savedRemember);
    }

    public void setRemember() {
        isChecked = remember.isChecked();
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (isChecked) {
            editor.putString("username", username);
            editor.putString("password", password);
            editor.putBoolean("isChecked", isChecked);
        } else {
            editor.clear();
        }
        editor.commit();
    }
}
