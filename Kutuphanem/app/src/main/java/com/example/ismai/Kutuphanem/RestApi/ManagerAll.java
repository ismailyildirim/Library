package com.example.ismai.Kutuphanem.RestApi;


import com.example.ismai.Kutuphanem.Model.Kitap;
import com.example.ismai.Kutuphanem.Model.KutuphanePojo;
import com.example.ismai.Kutuphanem.Model.KutuphanemPojo;
import com.example.ismai.Kutuphanem.Model.LoginPojo;

import java.util.List;

import retrofit2.Call;

public class ManagerAll extends BaseManager {
    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getOurInstance() {
        return ourInstance;
    }

    public Call<LoginPojo> giris(String ad, String sifre) {
        Call<LoginPojo> call = getRestApiClient().Login(ad, sifre);
        return call;
    }

    public Call<KutuphanePojo> kayit(String uyeId,String ad, String yazar,String yorum, String rating) {
        Call<KutuphanePojo> call = getRestApiClient().KutuphaneKayit(uyeId,ad, yazar,yorum,rating);
        return call;
    }

    public Call<List<KutuphanemPojo>> kutuphane(String uye_id) {
        Call<List<KutuphanemPojo>> call = getRestApiClient().Kutuphanem(uye_id);
        return call;
    }
    public Call<Result> kaydol(String kullaniciAdi,String sifre,String email) {
        Call<Result> call = getRestApiClient().kaydol(kullaniciAdi,sifre,email);
        return call;
    }

    public Call<Kitap> getir(String kitapId) {
        Call<Kitap> call = getRestApiClient().getir(kitapId);
        return call;
    }

    public Call<Kitap> guncelle(String kitapId,String yorum,String rating) {
        Call<Kitap> call = getRestApiClient().guncelle(kitapId,yorum,rating);
        return call;
    }

    public Call<Kitap> sil(String uyeId,String kitapId) {
        Call<Kitap> call = getRestApiClient().sil(uyeId,kitapId);
        return call;
    }




}
