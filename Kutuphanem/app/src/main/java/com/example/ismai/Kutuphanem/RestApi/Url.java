package com.example.ismai.Kutuphanem.RestApi;

public class Url {
    public static final String login = "login.php";
    public static final String kayit = "kutuphane.php";
    public static final String kutuphane = "kutuphanem.php";
    public static final String kaydol = "kaydol.php";
    public static final String duzenle = "duzenle.php";
    public static final String guncelle = "guncelle.php";
    public static final String sil = "kutuphanemdenkaldir.php";
}
